# Hispasec Web

Repositorio de la web de hispasec (hispasec.com)

## Índice

1. Instalación
2. Creación de templates
3. Servidor de pruebas
4. Actualización del código en desarrollo
5. Errores y soluciones

# 1. Instalación

```bash
# Instalación
git clone https://bitbucket.org/operacioncoconut/web.git --depth 1
cd web
virtualenv venv
source venv/bin/activate
npm install -g less
pip install -r requirements.txt
```

# 2. Creación de templates

Con esto se genera la web a partir de las plantillas de jinja2. 

```bash
sh compilebabel.sh
```
# 3. Servidor de pruebas

```bash
open http://127.0.0.1:8000/
python -m SimpleHTTPServer
```
## 4. Actualización del código en desarrollo

```bash
while true; do cp -r static/. output/static; python script.py; sleep 1; done
```

## 5. Errores y soluciones

### Errores de codificación al compilar la web

El build y el posterior deploy de la web puede fallar si en los templates de jinja2 se incluyen caracteres raros. Sustituirlos por su codificación HTML.

Ejemplo:

* á -> &aacute;
* é -> &eactute;
* ...

### Se pierden las traducciones

Puede pasar que se pierdan las traducciones de los .po. Bien porque haya fallado el build o por alguna otra razón. 
Para solucionarlo podemos hacer un checkout a un estado anterior de los .po en los que falte la traducción y volver a hacer el `./compilebabel.sh`.

Ejemplo:

`git checkout 788d1cd -- locale/en_US/LC_MESSAGES/messages.po locale/es_ES/LC_MESSAGES/messages.po locale/messages.pot`

`./compilebabel.sh`

(788d1cd es el hash de un commit que tiene los .po traducidos)
